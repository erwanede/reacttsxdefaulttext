export type CustomSettingValue = {
  extension: string;
  templateString: string[];
}[];
