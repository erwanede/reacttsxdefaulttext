import { CustomSettingValue } from "../types";

/**
 * Sorts the extensions in descending order based on the number of dots in the extension.
 *
 * @param {CustomSettingValue} extensions - An array of custom setting values representing the extensions to be sorted.
 * @return {CustomSettingValue} - The sorted array of custom setting values.
 */
export const sortExtensions = (extensions: CustomSettingValue) => {
  return extensions.sort(
    (a: CustomSettingValue[0], b: CustomSettingValue[0]) => {
      return b.extension.split(".").length - a.extension.split(".").length;
    }
  );
};
