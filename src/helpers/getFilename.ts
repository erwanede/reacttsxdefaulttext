import * as vscode from "vscode";

/**
 * Retrieves the filename from a given `vscode.Uri` object.
 *
 * @param {vscode.Uri} file - The `vscode.Uri` object representing the file.
 * @return {string} The filename without the file extension.
 */
export const getFilename = (file: vscode.Uri) => {
  let fileName = "Default";

  try {
    fileName = file.fsPath.split("/").reverse()?.[0]?.split(".")?.[0];
  } catch (e) {
    console.error(e);
  }

  return fileName;
};
