import * as vscode from "vscode";
import { getFilename } from "./getFilename";

const currentVariable: [string, (file: vscode.Uri) => string][] = [
  ["fileName", getFilename],
];

/**
 * Replaces variables in a file with their corresponding values.
 *
 * @param {vscode.Uri} file - The file to replace variables in.
 * @param {string} value - The value to replace the variables with.
 * @return {string} - The updated value with the variables replaced.
 */
export const replaceVariable = (file: vscode.Uri, value: string) => {
  let newValue = value;
  currentVariable.forEach(([variable, fn]) => {
    newValue = newValue.split("${" + variable + "}").join(fn(file));
  });
  return newValue;
};
