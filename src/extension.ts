// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from "vscode";
import { replaceVariable, sortExtensions } from "./helpers";
import { CustomSettingValue } from "./types";

let listener: vscode.Disposable | null;
// This method is called when your extension is activated
// Your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {
  const configuration = vscode.workspace.getConfiguration(
    "react-template-file"
  );
  const customSettingValue: CustomSettingValue | undefined =
    configuration.get("customSetting");
  if (!customSettingValue) {
    return;
  }
  listener = vscode.workspace.onDidCreateFiles(async (event) => {
    const sortCustomSettingValue = sortExtensions(customSettingValue);

    for (const file of event.files) {
      const index = sortCustomSettingValue.findIndex(({ extension }) =>
        file.fsPath.endsWith(`.${extension}`)
      );

      if (index >= 0) {
        const template = sortCustomSettingValue[index].templateString;
        const defaultText = template.join("\n");

        const finalTemplate = replaceVariable(file, defaultText);

        await vscode.workspace.fs.writeFile(file, Buffer.from(finalTemplate));
      }
    }
  });
}

export function deactivate() {
  listener?.dispose();
}
