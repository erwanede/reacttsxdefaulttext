# Change Log

## [Unreleased]

- Initial release

## [1.0.0] - 2023-05-09

### Added

- Add simple react.FC function to .tsx file
- Allow user to handle more extension

## 1.1.0

### Added

- Add fileName variable as ${fileName} in extension setting
- Handle extension priority
