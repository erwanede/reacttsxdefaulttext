# React Tsx Template File README

This extension add template to file with specific extension.
By default only tsx is handle with react.FC function template.

## Features

When you create .tsx file, by default, it will add to it:

![My Image](./images/code.png)

You can personalize tsx template or add another one by searching in workspace setting for "React-template-file"
-> Edit in settings.json

![My Image](./images/settings.png)

You can add item in react-template-file.customSetting array for handling more extension.
You can modify templateString. One item is one line in your file.

You can use ${fileName} in extension setting to get the filename of the file

### WARNING

If you modify settings.json, don't forget to reload window after your changes

## Known Issues

- None for now

## Release Notes

### 1.0.0

- Add simple react.FC function to .tsx file
- Allow user to handle more extension

### 1.1.0

- Add fileName variable as ${fileName} in extension setting
- Handle extension priority
